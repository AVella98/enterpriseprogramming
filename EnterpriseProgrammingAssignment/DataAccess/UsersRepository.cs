﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataAccess;
using System.Data.Entity.Migrations;

namespace DataAccess
{
    public class UsersRepository : ConnectionClass
    {
        public void AddUser(User u)
        {
            Entity.Users.Add(u);
            Entity.SaveChanges(); //whenever you are writing to database you need to call SaveChanges()
        }

        public User GetUserByid(string id)
        {
            return Entity.Users.Where(x => x.userId.ToString() == id).Single();
        }

        public void Save(User u)
        {
            User um = GetUserByid(u.userId.ToString());
            um.firstName = u.firstName;
            um.lastName = u.lastName;
            um.description = u.description;
            Entity.Users.AddOrUpdate(um);
            Entity.SaveChanges();
        }

        public void Delete(string id)
        {
            User u = GetUserByid(id);
            Entity.Users.Remove(u);
            Entity.SaveChanges();
        }
    }
}
