﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class PostsRepository : ConnectionClass
    {
        public List<Post> GetLastFivePosts()
        {
            return Entity.Posts.OrderByDescending(x => x.postId).Take(5).ToList();
        }

        public List<Post> GetLastFiveSpecificPosts(int categoryId)
        {
            return Entity.Posts.OrderByDescending(x => x.postId).Where(c => c.categoryId == categoryId).Take(5).ToList();
        }

        public List<Post> GetLastPost(int categoryId)
        {
            return Entity.Posts.OrderByDescending(x => x.postId).Where(c => c.categoryId == categoryId).Take(1).ToList();
        }

        public List<Post> GetPosts()
        {
            return Entity.Posts.ToList();
        }

        public List<Post> GetLastFourSpecificPosts(int categoryId)
        {
            return Entity.Posts.OrderByDescending(x => x.postId).Where(x => x.categoryId == categoryId).Skip(1).Take(4).ToList();
        }

        public Post GetPostByid(int id)
        {
            return Entity.Posts.Where(x => x.postId == id).Single();
        }

        public List<Post> GetPostByUserid(string id)
        {
            return Entity.Posts.Where(x => x.userId == new Guid(id)).ToList();
        }

        public void Delete(int postId)
        {
            Entity.Posts.Remove(GetPostByid(postId));
            Entity.SaveChanges();
        }

        public void AddPost(Post p)
        {
            Entity.Posts.Add(p);
            Entity.SaveChanges();
        }

        public void EditPost(Post p)
        {
            Post po = Entity.Posts.Where(x => x.postId == p.postId).Single();
            po.postTitle = p.postTitle;
            po.postTags = p.postTags;
            po.postContent = p.postContent;
            po.categoryId = p.categoryId;
            Entity.SaveChanges();
        }
    }
}
