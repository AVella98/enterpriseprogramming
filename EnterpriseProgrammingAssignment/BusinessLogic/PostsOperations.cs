﻿using Common;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
   public class PostsOperations
    {
        public List<Post> GetLastFivePosts()
        {
            return new PostsRepository().GetLastFivePosts();
        }

        public List<Post> GetLastPost(int categoryId)
        {
            return new PostsRepository().GetLastPost(categoryId);
        }

        public List<Post> GetPosts()
        {
            return new PostsRepository().GetPosts();
        }

        public List<Post> GetLastFourSpecificPosts(int categoryId)
        {
            return new PostsRepository().GetLastFourSpecificPosts(categoryId);
        }

        public List<Post> GetLastFiveSpecificPosts(int categoryId)
        {
            return new PostsRepository().GetLastFiveSpecificPosts(categoryId);
        }

        public Post GetPostByid(int id)
        {
            return new PostsRepository().GetPostByid(id);
        }

        public List<Post> GetPostByUserid(string id)
        {
            return new PostsRepository().GetPostByUserid(id);
        }

        public void Delete(int postId)
        {
            new PostsRepository().Delete(postId);
        }

        public void AddPost(Post p)
        {
            new PostsRepository().AddPost(p);
        }

        public void EditPost(Post p)
        {
            new PostsRepository().EditPost(p);
        }
    }
}
