﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DataAccess;

namespace BusinessLogic
{
    public class UsersOperations
    {
        public void AddUser(User u)
        {
            new UsersRepository().AddUser(u);
        }

        public void Save(User u)
        {
            new UsersRepository().Save(u);
        }

        public void Delete(string id)
        {
            new UsersRepository().Delete(id);
        }

        public User GetUserByid(string id)
        {
            return new UsersRepository().GetUserByid(id);
        }
    }
}
