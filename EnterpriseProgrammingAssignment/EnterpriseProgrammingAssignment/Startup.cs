﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EnterpriseProgrammingAssignment.Startup))]
namespace EnterpriseProgrammingAssignment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
