﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using BusinessLogic;

namespace EnterpriseProgrammingAssignment.Controllers
{
    public class AuthorController : Controller
    {
        // GET: Author
        public ActionResult Index(string id)
        {
            PostsOperations po = new PostsOperations();
            List <Post> post = po.GetPostByUserid(id);
            ViewData["post"] = post;
            return View();
        }
    }
}