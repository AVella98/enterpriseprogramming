﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLogic;
using System.Web.Mvc;

namespace EnterpriseProgrammingAssignment.Controllers
{
    public class DetailController : Controller
    {
        // GET: Detail
        public ActionResult Post(int id)
        {
            PostsOperations po = new PostsOperations();
            var post = po.GetPostByid(id);
            ViewData["post"] = post;
            return View();
        }
    }
}