﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EnterpriseProgrammingAssignment.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Csharp()
        {
            return View();
        }

        public ActionResult CSS()
        {
            return View();
        }

        public ActionResult Javascript()
        {
            return View();
        }

        public ActionResult MVC()
        {
            return View();
        }
    }
}