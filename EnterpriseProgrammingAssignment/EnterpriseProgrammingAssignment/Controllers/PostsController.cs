﻿using BusinessLogic;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using System.IO;

namespace EnterpriseProgrammingAssignment.Controllers
{
    public class PostsController : Controller
    {
        // GET: Posts
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {    
            return View();
        }

        [HttpPost]
        public ActionResult Create(Post p, HttpPostedFileBase image)
        {
            try
            {
                if(p.postTitle != null && p.postTags != null && p.postContent != null && p.postImage != null && p.categoryId != 0){
                    string fileName = Guid.NewGuid().ToString() + Path.GetExtension(image.FileName);
                    string path = Path.Combine(Server.MapPath("~/Uploads"), fileName);
                    string databasePath = Path.Combine("/Uploads", fileName);
                    image.SaveAs(path);
                    p.userId = new Guid(User.Identity.GetUserId());
                    p.postImage = databasePath;
                    PostsOperations po = new PostsOperations();
                    po.AddPost(p);
                }
                else
                {
                    ViewBag.Result = "Items are not filled in properly";
                }
            }
            catch (Exception e)
            {
                ViewBag.Result = e.Message;
            }
           return View();
        }

        public ActionResult EditPost(int id)
        {
            PostsOperations po = new PostsOperations();
            Post post = po.GetPostByid(id);
            ViewData["post"] = post;
            return View(post);
        }

        [HttpPost]
        public ActionResult EditPost(Post p)
        {
            try
            {
                if (p.postTitle != null && p.postTags != null && p.postContent != null && p.categoryId != 0)
                {
                    PostsOperations po = new PostsOperations();
                    po.EditPost(p);
                }
                else
                {
                    ViewBag.Result = "Items are not filled in properly";
                }
            }
            catch (Exception e)
            {
                ViewBag.Result = e.Message;
            }
            return RedirectToAction("Index", "Posts");
        }

        public ActionResult Delete(int id)
        {
            PostsOperations po = new PostsOperations();
            po.Delete(id);
            return RedirectToAction("Index", "Posts");
        }
    }
}