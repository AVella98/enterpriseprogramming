USE [master]
GO
/****** Object:  Database [EnterpriseAssignment]    Script Date: 23/05/2018 13:41:39 ******/
CREATE DATABASE [EnterpriseAssignment]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EnterpriseAssignment', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\EnterpriseAssignment.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EnterpriseAssignment_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\EnterpriseAssignment_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EnterpriseAssignment] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EnterpriseAssignment].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EnterpriseAssignment] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET ARITHABORT OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [EnterpriseAssignment] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EnterpriseAssignment] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EnterpriseAssignment] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET  ENABLE_BROKER 
GO
ALTER DATABASE [EnterpriseAssignment] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EnterpriseAssignment] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EnterpriseAssignment] SET  MULTI_USER 
GO
ALTER DATABASE [EnterpriseAssignment] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EnterpriseAssignment] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EnterpriseAssignment] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EnterpriseAssignment] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EnterpriseAssignment] SET DELAYED_DURABILITY = DISABLED 
GO
USE [EnterpriseAssignment]
GO
/****** Object:  Schema [ea]    Script Date: 23/05/2018 13:41:39 ******/
CREATE SCHEMA [ea]
GO
/****** Object:  Table [ea].[Categories]    Script Date: 23/05/2018 13:41:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ea].[Categories](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK__Categori__23CAF1D826C158FD] PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [ea].[Posts]    Script Date: 23/05/2018 13:41:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ea].[Posts](
	[postId] [int] IDENTITY(1,1) NOT NULL,
	[postTitle] [nvarchar](256) NOT NULL,
	[postImage] [nvarchar](500) NOT NULL,
	[postTags] [nvarchar](256) NOT NULL,
	[postContent] [nvarchar](max) NOT NULL,
	[categoryId] [int] NOT NULL,
	[userId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK__Posts__DD0C739A2EC3ECBA] PRIMARY KEY CLUSTERED 
(
	[postId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [ea].[Users]    Script Date: 23/05/2018 13:41:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ea].[Users](
	[userId] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[firstName] [nvarchar](256) NOT NULL,
	[lastName] [nvarchar](256) NOT NULL,
	[username] [nvarchar](256) NOT NULL,
	[description] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [ea].[Categories] ON 

INSERT [ea].[Categories] ([categoryId], [categoryName]) VALUES (1, N'Csharp')
INSERT [ea].[Categories] ([categoryId], [categoryName]) VALUES (4, N'CSS')
INSERT [ea].[Categories] ([categoryId], [categoryName]) VALUES (3, N'JavaScript')
INSERT [ea].[Categories] ([categoryId], [categoryName]) VALUES (2, N'MVC.Net')
SET IDENTITY_INSERT [ea].[Categories] OFF
SET IDENTITY_INSERT [ea].[Posts] ON 

INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (2, N'C# 1st', N'https://udemy-images.udemy.com/course/750x422/509454_0373.jpg', N'C#', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 1, N'5a9bf9e7-873e-40ef-9183-b6af0ccc4cf6')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (6, N'C# 2nd', N'https://udemy-images.udemy.com/course/750x422/509454_0373.jpg', N'C#', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 1, N'5a9bf9e7-873e-40ef-9183-b6af0ccc4cf6')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (8, N'C# 3rd', N'https://udemy-images.udemy.com/course/750x422/509454_0373.jpg', N'C#', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 1, N'5a9bf9e7-873e-40ef-9183-b6af0ccc4cf6')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (9, N'C# 4th', N'https://udemy-images.udemy.com/course/750x422/509454_0373.jpg', N'C#', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 1, N'5a9bf9e7-873e-40ef-9183-b6af0ccc4cf6')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (10, N'C# 5th', N'https://udemy-images.udemy.com/course/750x422/509454_0373.jpg', N'C#', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 1, N'5a9bf9e7-873e-40ef-9183-b6af0ccc4cf6')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (11, N'C# 6th', N'https://udemy-images.udemy.com/course/750x422/509454_0373.jpg', N'C#', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 1, N'5a9bf9e7-873e-40ef-9183-b6af0ccc4cf6')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (12, N'MVC.Net 1st', N'https://udemy-images.udemy.com/course/750x422/1181144_2c17.jpg', N'MVC.Net', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 2, N'66f35e26-ba5d-481e-908e-75ceb4ddc8ad')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (13, N'MVC.Net 2nd', N'https://udemy-images.udemy.com/course/750x422/1181144_2c17.jpg', N'MVC.Net', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 2, N'66f35e26-ba5d-481e-908e-75ceb4ddc8ad')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (14, N'MVC.Net 3rd', N'https://udemy-images.udemy.com/course/750x422/1181144_2c17.jpg', N'MVC.Net', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 2, N'66f35e26-ba5d-481e-908e-75ceb4ddc8ad')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (15, N'MVC.Net 4th', N'https://udemy-images.udemy.com/course/750x422/1181144_2c17.jpg', N'MVC.Net', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 2, N'66f35e26-ba5d-481e-908e-75ceb4ddc8ad')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (16, N'MVC.Net 5th', N'https://udemy-images.udemy.com/course/750x422/1181144_2c17.jpg', N'MVC.Net', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 2, N'66f35e26-ba5d-481e-908e-75ceb4ddc8ad')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (17, N'MVC.Net 6th', N'https://udemy-images.udemy.com/course/750x422/1181144_2c17.jpg', N'MVC.Net', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 2, N'66f35e26-ba5d-481e-908e-75ceb4ddc8ad')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (18, N'JavaScript 1st 2nd', N'https://i.pinimg.com/originals/f9/7d/35/f97d35b8c6c61d0a99d048bc892c3fee.jpg', N'JavaScript', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 3, N'bc78434f-2437-4483-88da-bb564980960c')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (21, N'JavaScript 2nd', N'https://i.pinimg.com/originals/f9/7d/35/f97d35b8c6c61d0a99d048bc892c3fee.jpg', N'JavaScript', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 3, N'bc78434f-2437-4483-88da-bb564980960c')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (22, N'JavaScript 3rd', N'https://i.pinimg.com/originals/f9/7d/35/f97d35b8c6c61d0a99d048bc892c3fee.jpg', N'JavaScript', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 3, N'bc78434f-2437-4483-88da-bb564980960c')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (23, N'JavaScript 4th', N'https://i.pinimg.com/originals/f9/7d/35/f97d35b8c6c61d0a99d048bc892c3fee.jpg', N'JavaScript', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 3, N'bc78434f-2437-4483-88da-bb564980960c')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (24, N'JavaScript 5th', N'https://i.pinimg.com/originals/f9/7d/35/f97d35b8c6c61d0a99d048bc892c3fee.jpg', N'JavaScript', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 3, N'bc78434f-2437-4483-88da-bb564980960c')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (26, N'CSS 1st', N'https://tutsgalaxy.com/wp-content/uploads/2018/03/css.jpg', N'CSS', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 4, N'2be8b59f-5b4e-4b2e-aee2-cb67b1025b95')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (28, N'CSS 2nd', N'https://tutsgalaxy.com/wp-content/uploads/2018/03/css.jpg', N'CSS', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 4, N'2be8b59f-5b4e-4b2e-aee2-cb67b1025b95')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (30, N'CSS 3rd', N'https://tutsgalaxy.com/wp-content/uploads/2018/03/css.jpg', N'CSS', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 4, N'2be8b59f-5b4e-4b2e-aee2-cb67b1025b95')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (33, N'CSS 4th', N'https://tutsgalaxy.com/wp-content/uploads/2018/03/css.jpg', N'CSS', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 4, N'ee01b4ec-b435-4996-ae37-e41f831c2c65')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (35, N'CSS 5th', N'https://tutsgalaxy.com/wp-content/uploads/2018/03/css.jpg', N'CSS', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 4, N'ee01b4ec-b435-4996-ae37-e41f831c2c65')
INSERT [ea].[Posts] ([postId], [postTitle], [postImage], [postTags], [postContent], [categoryId], [userId]) VALUES (36, N'CSS 6th', N'https://tutsgalaxy.com/wp-content/uploads/2018/03/css.jpg', N'CSS', N'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vitae neque iaculis enim venenatis scelerisque quis vitae purus. Morbi id elit vel felis vestibulum pellentesque eu nec ligula. Sed urna metus, malesuada eu dignissim vitae, posuere sed tellus. Pellentesque mi justo, semper ut lobortis vitae, venenatis eu massa. Vestibulum sollicitudin.', 4, N'ee01b4ec-b435-4996-ae37-e41f831c2c65')
SET IDENTITY_INSERT [ea].[Posts] OFF
INSERT [ea].[Users] ([userId], [firstName], [lastName], [username], [description]) VALUES (N'66f35e26-ba5d-481e-908e-75ceb4ddc8ad', N'Peppi', N'Azzopardi', N'peppia', N'I am Peppi Azzopardi. I am from Malta. This is my hobby.')
INSERT [ea].[Users] ([userId], [firstName], [lastName], [username], [description]) VALUES (N'5a9bf9e7-873e-40ef-9183-b6af0ccc4cf6', N'André', N'Vella', N'andrev', N'I am André Vella. Coding is fun.')
INSERT [ea].[Users] ([userId], [firstName], [lastName], [username], [description]) VALUES (N'bc78434f-2437-4483-88da-bb564980960c', N'Mario', N'Vella', N'mariov', N'I am Mario Vella')
INSERT [ea].[Users] ([userId], [firstName], [lastName], [username], [description]) VALUES (N'2be8b59f-5b4e-4b2e-aee2-cb67b1025b95', N'Sandra', N'Vella', N'sandrav', N'I am Sandra Vella')
INSERT [ea].[Users] ([userId], [firstName], [lastName], [username], [description]) VALUES (N'ee01b4ec-b435-4996-ae37-e41f831c2c65', N'Christian', N'Mercieca', N'christianm', N'I am Christian Mercieca')
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Categori__37077ABDAC29ED93]    Script Date: 23/05/2018 13:41:39 ******/
ALTER TABLE [ea].[Categories] ADD  CONSTRAINT [UQ__Categori__37077ABDAC29ED93] UNIQUE NONCLUSTERED 
(
	[categoryName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Posts__B2369610718C193A]    Script Date: 23/05/2018 13:41:39 ******/
ALTER TABLE [ea].[Posts] ADD  CONSTRAINT [UQ__Posts__B2369610718C193A] UNIQUE NONCLUSTERED 
(
	[postTitle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Users__F3DBC572A1A7189E]    Script Date: 23/05/2018 13:41:39 ******/
ALTER TABLE [ea].[Users] ADD UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [ea].[Posts]  WITH CHECK ADD  CONSTRAINT [FK__Posts__categoryI__03F0984C] FOREIGN KEY([categoryId])
REFERENCES [ea].[Categories] ([categoryId])
GO
ALTER TABLE [ea].[Posts] CHECK CONSTRAINT [FK__Posts__categoryI__03F0984C]
GO
ALTER TABLE [ea].[Posts]  WITH CHECK ADD  CONSTRAINT [FK__Posts__userId__04E4BC85] FOREIGN KEY([userId])
REFERENCES [ea].[Users] ([userId])
GO
ALTER TABLE [ea].[Posts] CHECK CONSTRAINT [FK__Posts__userId__04E4BC85]
GO
USE [master]
GO
ALTER DATABASE [EnterpriseAssignment] SET  READ_WRITE 
GO
